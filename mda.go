package main

import(
	"fmt"
	"encoding/json"
	"io/ioutil"
	"database/sql"
	_ "./pq"
	"flag"
	"net"
	"./telnet"
	"strings"
	"time"
	"strconv"
)

type Config struct{
	Port		string
	Server		string
	UseDB		bool
	DbUser		string
	DbPassw		string
	DbServer	string
	DbName		string
	DbSSLmode	string
	Kurse		[]string
}

var(
	config = new(Config)
	db *sql.DB
	version = "0.0.3"
	lizenz = "BSD"
	herausgeber = "Michael Heinrich, Ludwig Leicht"
	returnChan chan string = make(chan string)

)


func main(){

	// Überprüfe ob Versionsnummer ausgegeben werden soll oder ob Server gestartet werden soll
	if versionAusgeben() {return}

	// Lese Konfiguration aus Datei
	readConfig()

	if config.UseDB {
		// Verbindung mit Datenbank herstellen
		db = getDatabase()
		// Überprüfe die Datenbankverbindung
		if !checkDatabaseConnection(db){
			fmt.Printf("Fehlerhafte Datenbankverbindung\n")
			return
		}
	}


	// Die Abfragen der einzelnen Kurse anstoßen.
	for _, kurs := range config.Kurse {
		//TODO Channels bearbeiten
		go abfragen(kurs)
	}


	for{
		select {
			case value := <-returnChan:

			if value == "beende"{
			fmt.Printf("Es gab einen Abbruchbefehl in einer Go Routine!!!!\n")
			return
			}
		}
	}

}


func abfragen(kurspaar string){
	// Verbindung mit Telnet Server herstellen

	tel := getTelnetVerbindung()
	var lastKurs string

	for{
		time.Sleep(1000 * time.Millisecond)
		aktKurs, noerr := holeKurs(tel, kurspaar)
		if noerr == true {
			kurs := aktKurs[0]+"|"+aktKurs[1]+"|"+aktKurs[2]+"|"+aktKurs[3]+"|"+aktKurs[4]+"|"+aktKurs[5]


			if lastKurs == "" || lastKurs != kurs{
				fmt.Printf(kurs)

				if (config.UseDB){
					ins := createSqlInsertBefehl(aktKurs)
					if(ins != ""){
						// Schreibe in die Datenbank
						_, err := db.Exec(ins)
						if err != nil{
							fmt.Println("konnte nicht in datenbank schreiben")
						}
					}else{
						fmt.Printf("Error beim erstellen des SQL Befehls")
					}
				}
				lastKurs = kurs
			}


		}else{
			fmt.Printf("Es ist ein fehler aufgetreten. beim holen des kurses" )
			//TODO WAS IST WENN TELNETVERBINDUNG ABBRICHT 
				// RECONNECT?
		}


	}
}

// Generiert den Insert Befehl für die Datenbank
func createSqlInsertBefehl(aktKurs []string) (string) {
	// Trend 1 == up
	//	 2 == down
	trendId := -1
	if(aktKurs[0] == "up "){
		trendId = 1
	}
	if(aktKurs[0] == "down "){
		trendId = 2
	}

	kursname := ""

	q := `SELECT "kId" FROM "KursName" WHERE "kName" = '`+aktKurs[1]+`'`

	reli, err := db.Query(q);
	if err != nil{
		fmt.Println("Es hat einen Fehler bei der Datenbank gegeben. / createSqlInsertBefehl")
		fmt.Println(err.Error())
		return ""
	}

	//var names string
	for reli.Next(){
		_ = reli.Scan(&kursname)
	}


	// TIMESTAMP umbauen
	tmp := strings.NewReplacer(".", "-")
	date := tmp.Replace(aktKurs[4])
	uhrzeit := aktKurs[5]
	tmpleng := len(uhrzeit)
	tmpleng = tmpleng -1
	uhrzeit = uhrzeit[0:tmpleng]
	time := date + " " + uhrzeit
	zeitstempel := `TIMESTAMP '`+time+`'`


	insertbefehl := "INSERT INTO \"Kurs\" (\"kNr\", \"time\", \"bKurs\", \"sKurs\", \"trendNr\") VALUES ("+ string(kursname)+", "+zeitstempel+", "+string(aktKurs[2])+", "+string(aktKurs[3])+", "+strconv.Itoa(trendId)+")"


	if trendId != -1 {return insertbefehl}
	return ""
}


// Holt aktuellen Kurs vom Telnet server und gibt ihn als Stringarray zurück
// [0] up oder down
// [1] Kursbezeichnung
// [2] Kaufkurs
// [3] Verkaufkurs
// [4] Datum
// [5] Uhrzeit
func holeKurs(tel *telnet.Conn, kurs string) ([]string, bool){

	/*
	WINFO
	MetaTrader 4 Server 4.00 build 452
	Sucden Financial Ltd.
	QUOTES-EURUSD,
	up EURUSD 1.29997 1.30017 2013.03.08 21:32:45
	2013.03.08 21:32:45
	QUOTES-EURUSD,
	up EURUSD 1.29999 1.30019 2013.03.08 21:32:53
	2013.03.08 21:32:53
	QUOTES-EURUSD,
	up EURUSD 1.29999 1.30019 2013.03.08 21:32:53
	2013.03.08 21:32:53

*/

	// _, err := tel.Write([]byte("QUOTES-EURUSD,\n"))
	_, err := tel.Write([]byte("QUOTES-"+kurs+",\n"))
	if err != nil{
		fmt.Printf("Kurs konnte nicht ordnungsgemäß abgerufen werden\n" + err.Error() + "\n")
	}
	line, err := tel.ReadUntil("\n")
	_, err =  tel.ReadUntil("\n")
	if err != nil{
		fmt.Printf("Konnte Zeile nicht lesen\n"+err.Error() )
	}
	if strings.Contains(string(line), "up") || strings.Contains(string(line), "down") {
		return strings.SplitAfter(string(line), " "), true
	}
	return nil, false
}


// Baut eine Verbindung zum Telnet Server auf
// Gibt die Verbindung zurück
func getTelnetVerbindung() *telnet.Conn {

	_, err := net.LookupIP(config.Server)
	if err != nil {
		panic("\x1b[31mKonnte angegebenen MT-Server nicht finden\x1b[0m\n")
	}

	tel, err := telnet.Dial("tcp4", config.Server+":"+config.Port)
	if err != nil{
		panic("Konnte keine Verbindung mit MT-Server aufbauen\n" + err.Error() )
	}

	_, err = tel.Write([]byte("WINFO\n"))
	// _, err = tel.Write([]byte("QUOTES-EURUSD,\n")) 

	// intro abholen und evtl ausgeben

	for p:=0; p<2; p++ {
		xy, _ := tel.ReadUntil("\n")
		if config.UseDB{
			fmt.Printf("%s", xy)
		}
	}

	return tel

}

// Gibt Version, Herausgeber und Lizenz auf die Standartausgabe aus
func versionAusgeben() bool{
	flag.Parse()
	args := flag.Args()
	if len(args) > 0 {
		if args[0] == "version"{
		fmt.Printf("\x1b[31mVersion: "+version+"\nHerausgeber: "+herausgeber+"\nLizenz: "+lizenz+"\x1b[0m\n")
		return true
		}
	}
	return false
}

// Verbindung zur Datenbank herstellen
func getDatabase() *sql.DB{

	//db, err := sql.Open("postgres", "user=postgres password=postgres dbname=postgres sslmode=verify-full")
	db, err := sql.Open("postgres","host="+config.DbServer+ " user=" + config.DbUser + " password=" + config.DbPassw + " dbname=" + config.DbName + " sslmode=" + config.DbSSLmode)
	if err != nil{
		fmt.Println(err.Error())
		return nil
	}
	return db;
}

// Überprüft ob eine Datenbankverbindung besteht indem der Wert "down" aus der Tabelle "Trend" zurückgegeben werden soll
func checkDatabaseConnection ( datenbank *sql.DB ) bool{

	q := `SELECT "name" FROM "Trend" WHERE "tId" = 2 `
	rel, err := datenbank.Query(q);
	if err != nil{
		fmt.Printf("Konnte Datenbank nicht lesen!\n")
		return false
	}
	var value string
	for rel.Next(){
		_ = rel.Scan(&value)
	}
	if value != "down" {
		panic("\nDatenbank ist korrupt\n")
	}
	return true
}

// Auslesen der Konfigurationsdatei und einordnen der Werte in config
func readConfig (){

	configRaw, _ := ioutil.ReadFile("config.json")
	err := json.Unmarshal(configRaw, config)
	if err != nil{
		fmt.Printf(err.Error())
		panic("\nKonnte Konfiguration nicht laden")
	}
}

